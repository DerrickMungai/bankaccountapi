package com.bankaccount.Service;

import com.bankaccount.Entity.transaction;
import com.bankaccount.Data.TransactionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;


@Service
public class TransactionService {
    @Autowired
    private TransactionData transactionData;

    public Collection<transaction> getAllTransaction(){
        return this.transactionData.getAllTransaction();
    }

    public String depositfunds(transaction deposit) {
        if(deposit.getAmount()>40000 ){
            return "1";
        }if(this.transactionData.getTransactionCount("Deposit")>=4 ){
            return "2";
        }if(deposit.getAmount()+this.transactionData.getTransactionAmount("Deposit")>150000){
            int max=150000-getBalance();
            return "3";
        }else {
            return transactionData.depositfunds(deposit);
        }
    }

    public String withdrawfunds(transaction withdraw) {
        if(getBalance()<withdraw.getAmount()){
            return "1";
        }else if(this.transactionData.getTransactionCount("Withdrawal")>=3){
            return "2";
        }else if(this.transactionData.getTransactionAmount("Withdrawal")-withdraw.getAmount()<-50000){
            return "3";
        }else if(withdraw.getAmount()>20000){
            return "4";
        }else {
            return transactionData.withdrawfunds(withdraw);
        }
    }

    public int getBalance(){
       return transactionData.getBalance();
    }
}
